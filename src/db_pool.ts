var mariadb = require('mariadb');
// const { connect } = require('../app');
require('dotenv').config()

// create a new connection pool
const pool = mariadb.createPool({
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database
});

module.exports = {
    getConnection: async function () {
        return await pool.getConnection();
    }
}
