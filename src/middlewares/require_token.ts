import express from "express"
import { IncomingMessage, OutgoingMessage } from "node:http";
const jwt = require("jsonwebtoken");

module.exports = function authenticateToken(req : IncomingMessage, res : any, next : any) {
    // Gather the jwt access token from the request header
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401) // if there isn't any token
  
    jwt.verify(token, process.env.secret as string, (err: any, data: any) => {
      // console.log(err)
      if (err) return res.sendStatus(403)
      res.locals.userid = data.uid
      next() // pass the execution off to whatever request the client intended
    })
  }