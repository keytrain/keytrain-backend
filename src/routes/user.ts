import express from "express"
import { LoginBody } from "../interfaces"
var require_token = require("../middlewares/require_token")
var router = express.Router();

var pool = require("../db_pool")
const jwt = require("jsonwebtoken");

const securePassword = require('secure-password')
const pwd = securePassword()


router.post('/login', async (req, res) => {
    let conn;
    try {
        conn = await pool.getConnection();
        const body: LoginBody = req.body;
        
        var hash_row = (await conn.query("SELECT hash from users where name = ?", [body.username]))[0]
        if(hash_row === undefined)
            return res.send({ status: "error", msg: "Incorrect credentials!" })
        
        const result = await pwd.verify(Buffer.from(body.password), Buffer.from(hash_row.hash))

        if (result === securePassword.VALID || result === securePassword.VALID_NEEDS_REHASH) {
            var uid = (await conn.query("SELECT id from users WHERE name = ?", [body.username]))[0].id
            var token = jwt.sign({ uid }, process.env.secret);
            return res.send({ status: "ok", msg: "Successful login!", token })
        }
        else {
            return res.send({ status: "error", msg: "Incorrect credentials!" })
        }


    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.release();
    }
});

router.post('/users', async (req, res, next) => {
    let conn;
    try {
        conn = await pool.getConnection();
        const body: LoginBody = req.body;


        const hash = await pwd.hash(Buffer.from(body.password));
        var insert = await conn.query("INSERT INTO users (name, hash) VALUES (?, ?)", [body.username, hash])
        // .catch((err: any) => { 
        //     res.send({status: "error", msg: `User '${body.username}' already exists`});
        //     next()
        // })
        return res.send({ status: "ok", msg: `User '${body.username}' has been successfully created.` })

    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.release();
    }
});

router.get('/check', require_token, async (req, res) => {
    // var token = req.body.token;
    return res.send("cuc")
});

module.exports = router;