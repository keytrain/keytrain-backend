import express from "express"
var router = express.Router();
import {LessonStats} from "../interfaces"
const time = require("strftime");
var pool = require("../db_pool");
var require_token = require("../middlewares/require_token")
var io = require("../socket_io")

router.post('/enter', require_token, async (req, res) => {
    let conn;
    try {
        conn = await pool.getConnection();
        const ls: LessonStats = req.body;
        // console.log(ls);
        if (ls.text.length != ls.times.length) {
            return res.send('Error: Supplied times array length does not match the length of the text!')
        }
        var stamp = time("%Y-%m-%d %H:%M:%S");

        var lessons_insert = await conn.query("INSERT INTO `lessons` (`dictionaryid`, `text`, `userid`, timestamp) VALUES (?, ?, ?, ?)", [1, ls.text, res.locals.userid, stamp]);
        if (lessons_insert.insertId == 0)
            return res.send({ status: "error", msg: "Insertion failed!" });

        for (let i = 0; i < ls.text.length; i++) {
            const letter = ls.text[i];
            const ms = ls.times[i];
            const miss = ls.misses.includes(i);
            // console.log(`${i}: ${letter} - ${ms} ms - ${miss}`)
            var insert = await conn.query(
                "INSERT INTO `letters` (`textid`, `index`, `letter`, `mistake`, `ms`) VALUES (?, ?, ?, ?, ?)",
                [lessons_insert.insertId, i, ls.text[i], miss, ms]);
            if (insert.insertId == 0)
                return res.send({ status: "error", msg: "Insertion failed!" });
        }

        io.update(res.locals.userid);
        return res.send("Successful upload!")
    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.release();
    }
});


module.exports = router;