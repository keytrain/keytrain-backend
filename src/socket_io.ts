const last20_query = `
SELECT ROUND(12000 / (sum(ms) / (LENGTH(TEXT) -1)), 2) as avg_wpm, timestamp
FROM lessons INNER JOIN letters 
ON (letters.textid = lessons.id)
WHERE timestamp IS NOT NULL
group by textid
ORDER BY timestamp DESC
LIMIT 20`

const header_query = `
SELECT ROUND(avg(wpm), 2) as average, ROUND(max(wpm), 2) as max, sum(misses) / sum(total) as error_rate FROM (
    SELECT 12000 / (sum(ms) / (LENGTH(TEXT) -1)) as 'wpm', sum(mistake) as misses, count(letters.id) as total
    FROM lessons INNER JOIN letters 
    ON (letters.textid = lessons.id)
    WHERE timestamp IS NOT NULL
    group by textid) T1`

//socket_id - user_id binding
var bindings = {}

var pool = require("./db_pool")
const jwt = require("jsonwebtoken");

const io = require("socket.io")(3000, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"]
    }
});

io.on('connection', async (socket: SocketIO.Socket) => {
    // console.log("connection");


    socket.on('bootstrap', async () => {
        let conn;
        try {
            conn = await pool.getConnection();
            socket.emit('wpm_stats', await conn.query(last20_query));
            socket.emit('header_stats', await conn.query(header_query))

        } catch (err) {
            throw err;
        } finally {
            if (conn) conn.release();
        }
    });
    socket.on('bind', (token) => {
        if(token == ""){
            delete bindings[socket.id]
            return;
        }

        jwt.verify(token, process.env.secret as string, (err: any, data: any) => {
            // console.log(err)
            if (err) {
                console.log("Invalid token received from client!")
            }

            var userid = data.uid
            bindings[socket.id] = userid
        })
    });

    socket.on('disconnect', (token) => {
        delete bindings[socket.id]
    });

});


//sends up to date stats to every connected socket that has the given socket id
async function update(userid: Number) {
    for (const [socket_id, bound_user_id] of Object.entries(bindings)) {
        if (bound_user_id == userid) {
            var socket = io.of('/').sockets.get(socket_id);
            let conn;
            try {
                conn = await pool.getConnection();
                socket.emit('wpm_stats', await conn.query(last20_query));
                socket.emit('header_stats', await conn.query(header_query));

            } catch (err) {
                throw err;
            } finally {
                if (conn) conn.release();
            }

        }
    }
}

export {
    io,
    update
}