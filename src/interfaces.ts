
interface LessonStats {
    text: string;
    times: number[];
    misses: number[];
    dictionary: string;
}

interface LoginBody {
    username: string;
    password: string;
}

export{
    LessonStats,
    LoginBody
}