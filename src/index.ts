import express, { text } from "express";
const app = express();
const PORT = 8000;
const helmet = require("helmet");
const time = require("strftime");
require('dotenv').config()

var pool = require("./db_pool")
var cors = require('cors')

app.use(helmet());
app.use(cors());

var io = require('./socket_io').io

app.use(express.json());
app.use(function (req, res, next) {
    res.locals.io = io;
    next();
});


app.get('/', (req, res) => {
    return res.send('Express + TypeScript Server')
});

app.use('/', require('./routes/stats'));
app.use('/', require('./routes/user'));

app.listen(PORT, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});